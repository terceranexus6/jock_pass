
## Name of the contribution

Short description

Issue (if it exists): 

Check list:

- [ ] I checked it works in my machine
- [ ] Is part of an issue
- [ ] Is a completed merge request

Additional notes

_Example:_

## Adding a new language

I added a python version

Issue: 1

- [X] I checked it works in my machine
- [X] Is part of an issue
- [ ] Is a completed merge request

Hi! so I added a python version, although I think I can improve it in the future. For now I'm sending the current version.



